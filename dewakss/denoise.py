import scanpy as _sc
import scipy as _sp
import numpy as _np
from anndata import AnnData as _AnnData
from scipy.sparse import issparse as _issparse
import scipy.sparse as _scs
from sklearn.utils.validation import check_is_fitted as _check_is_fitted
import warnings as _warnings
from sklearn.metrics import r2_score as _r2
from sklearn.base import BaseEstimator as _BaseEstimator, TransformerMixin as _TransformerMixin
from scvelo.preprocessing.neighbors import select_connectivities as _select_connect, select_distances as _select_dist
import time as _time
import matplotlib.pyplot as _plt
from sklearn.model_selection import ShuffleSplit as _ShuffleSplit

MODE_FUNC = {'connectivities': _select_connect,
             'distances': _select_dist}


class DEWAKSS(_BaseEstimator, _TransformerMixin):

    def __init__(self, use_layer='X', create_layer='Ms', mode='connectivities', iterations=10, weighted=True,
                 init_thresholding=False, thresholding=False, keep0=False, init_diag=0, set_diag=0, n_neighbors=None,
                 symmetrize=False, denoise_type='mean', run2best=True, verbose=True, memoryefficient=True, subset=False,
                 random_state=42, max_dense=0.4, decay=1):

        """DEWAKSS does Denoising of Expression data with
        Weighted Affinity Kernel and Self Supervision.

        DEWAKSS accepts an Affinity Kernal matrix to determine what
        datapoints should be averaged over and uses noise2self to
        determine what level of denoising most informative.

        Parameters marked with [*] will only affect the input if
        data is an AnnData object [pip insall anndata]

        :param use_layer: [*] determine what layer should be used as the data. Default 'X'
        :param create_layer: [*] name of layer in AnnData object to be create. Default 'Ms'
        :param n_neighbors: if data is AnnData object will use the number used for calculating neighbours,
                            else will estimate median or use supplied if not None
        :param mode: [*] 'connectivities' or 'distances'. If data is AnnData will select the appropriate measure
                         for the Affinity graph. Default: 'connectivities'.
        :param iterations: Number of iterations to perform if run2best is False. Will be ignored if supplied in the fit
            function.
        :param weighted: [*] If False will use the initial Affintiy matrix with equally weighted neighbour influences.
            This may change during fitting.
        :param init_thresholding: Should the affinity matrix be thresholded on min influence in the initial step.
        :param thresholding: Should the affinity matrix be continually thresholded on min influence for all steps.
        :param keep0: Preserve 0 entries in the data matrix during iteration and transformation, Default False.
        :param init_diag: Explicitly set the diagonal entries of the initial affinity matrix to this value
                          before transforming to right stochastic matrix. Default 0
        :param set_diag: Explicitly set the diagonal entries of the affinity matrix at each step to this value
                         before transforming to right stochastic matrix. Default 0
        :param symmetrize: Transform the initial affinity matrix to a symmetric matrix by summing C + C.T. Default False
        :param denoise_type: what operation should be performed to denoise, ['mean', 'median']. Default 'mean'.
        :param run2best: Should we stop as soon as optimal is reached. Default True.
        :param verbose: Should we print progress at each iteration step. Should we annotate the plot. Default True.
        :param memoryefficient: Should we try to be memory efficient by keeping data sparse. Default True
        :param subset: Should we select a subset of data to fit on. Accepts float or int or bool. Default False.
        :param random_state: For random operations what random state should be used, Default 42
        :param max_dense: At what density should we transform our Affinity matrix to dense. Default 0.4
        :param decay: Parameter that tries to mimic MAGIC decay parameter. However it is not the same. Set at own risk.
            Default 1
        :returns: self
        :rtype: DEWAKSS

        """

        super(DEWAKSS, self).__init__()

        self.random_state = random_state
        self.memoryefficient = memoryefficient
        self.subset = subset
        self.max_dense = max_dense
        self.use_layer = use_layer
        self.create_layer = create_layer
        self.mode = mode
        self.set_diag = set_diag
        self.init_diag = init_diag
        self.decay = decay
        self.weighted = weighted

        self.denoise_type = denoise_type
        self.run2best = run2best
        self.verbose = verbose

        self.iterations = iterations
        self.init_thresholding = init_thresholding
        self.thresholding = thresholding
        self.keep0 = keep0
        self.symmetrize = symmetrize
        self.n_neighbors = n_neighbors

    def _vp(self, *args, **kwargs):
        if self.verbose:
            print(*args, **kwargs)
        else:
            pass

    def fit(self, data, iterations=None, copy=True):
        """Fit function for training DEWAKSS

        :param data: AnnData object or matrix
        :param iterations: If this is set it hard sets the number of iterations ignoring DEWAKSS settings and stores the
            opt_connectivities as the final iteration instead of the best one. Default None.
        :returns: self
        :rtype: DEWAKSS object

        """

        data = DEWAKSSData(data, layer=self.use_layer, copy=copy)
        self._parse_input_anndata(data)

        if not self.memoryefficient:
            data.make_dense()

        if self.subset != 0:
            data = self._subset(data, self.subset, random_state=self.random_state)

        start_time = _time.time()

        prediction = self._iterate(data, iterations=iterations)

        self._extime = _time.time() - start_time
        self.prediction_ = prediction

        return self

    def _parse_input_anndata(self, data):
        """
        Parse AnnData
        :param data:
        :return:
        """

        if self.n_neighbors is None:
            self.n_neighbors = data.n_neighbors

        self.connectivities = self.get_connectivities(data)

    @staticmethod
    def _row_stochastic(connect_data):
        """
        Normalizes each row to the row sum in place
        :param connect_data:  DEWAKKSData
            Connectivity data
        """

        # Row-wise sum
        row_sum = connect_data.sum(axis=1)
        # Fix zeros
        row_sum[row_sum == 0] = 1
        # Multiply row-wise by 1 / row_sum
        connect_data.multiply(1 / row_sum[:, _np.newaxis])

    def get_connectivities(self, adata):

        n_neighbors = self.n_neighbors

        c_matrix = DEWAKSSData(adata.uns['neighbors'][self.mode].copy())

        # Re-run scvelo neighbors if n_neighbors isn't set (or is lower than what is currently set)
        if n_neighbors is not None and n_neighbors < adata.uns['neighbors']['params']['n_neighbors']:
            c_matrix.X = MODE_FUNC[self.mode](c_matrix.X, n_neighbors)

        if self.weighted:
            # Preprocess distances or connectivities
            if self.mode == "distances":
                # Normalize to mean non-zero row values
                c_matrix.multiply(1. / c_matrix.mean_of_nnz(axis=1))

                # Element-wise power for decay
                if self.decay != 1:
                    c_matrix.X = c_matrix.X.power(self.decay)

                # Element-wise e ** -x
                c_matrix.exp(modifier_value=-1)
            else:
                # Element-wise power for decay
                if self.decay != 1:
                    c_matrix.X = c_matrix.X.power(self.decay)

            c_matrix.fill_diagonal(self.init_diag)
            self._row_stochastic(c_matrix)

        self._threshold_knn(c_matrix, self.init_thresholding)

        if self.symmetrize:
            c_matrix.make_symmetric()

        if not self.memoryefficient:
            c_matrix.make_dense()

        return c_matrix

    @staticmethod
    def _threshold_knn(connectivities, thresholding=False):
        """
        :param connectivities: DEWAKSSData
        :param thresholding:
        :return:
        """

        # Do not threshold just pass by.
        if isinstance(thresholding, bool) and (not thresholding):
            return

        # set to scale of contribution depending on number of total nodes
        elif isinstance(thresholding, bool) and thresholding:
            connectivities.threshold(1 / connectivities.shape[0])

        # If threshold is > 1, use the integer value as number of maximum neighbors
        elif thresholding > 1:
            thresholding = int(thresholding)
            connectivities.trim_to_nnz(thresholding)

        # set to minimum contribution
        else:
            connectivities.threshold(thresholding)

    @staticmethod
    def _apply_smoothing(expression, connectivities, denoise_type, keep0=False):
        """
        Smooth the data using a connectivity matrix
        Returns new data; does not modify in place
        :param expression: DEWAKSSData
        :param connectivities: DEWAKSData
        :param denoise_type: str
        :param keep0: bool
            Restore sparsity
        :return: DEWAKSSData
        """

        if keep0:
            Xtmp = expression.X == 0

        if denoise_type == 'mean':
            X_out = connectivities.dot(expression.X)
        elif denoise_type == 'median':
            raise NotImplementedError

        if keep0:
            X_out.multiply(Xtmp)

        return X_out

    @staticmethod
    def _renormalize(connectivities, set_diag=True, thresholding=None, weighted=True):

        if set_diag:
            connectivities.fill_diagonal()

        DEWAKSS._threshold_knn(connectivities, thresholding=thresholding)

        if not weighted:
            connectivities.astype(bool)
            connectivities.astype(float)

        DEWAKSS._row_stochastic(connectivities)

    @staticmethod
    def _subset(data, train_size, random_state=1):
        rs = _ShuffleSplit(1, train_size=train_size, random_state=random_state)
        train_index, test_index = next(rs.split(_scs.csr_matrix(data.X.shape).T))
        return DEWAKSSData(data.X[:, train_index])

    def _iterate(self, expression_data, iterations=None):

        prediction = {}

        if iterations is None and self.run2best:
            fixed_iterations = False
            iterations = self.iterations
        elif iterations is None:
            fixed_iterations = True
            iterations = self.iterations
        else:
            fixed_iterations = True

        current_connectivities = self.connectivities.copy()
        smoothed_expression = None

        last_iteration_mse = 0
        optimal_mse = None

        for i in range(iterations):

            mse = expression_data.mse(true_values=smoothed_expression)
            r2 = expression_data.r2(true_values=smoothed_expression)
            prediction[i] = [mse, r2]

            if optimal_mse is None:
                optimal_mse = mse

            self._vp(f'Progress = {100*i/(iterations-1):.3f}%, i={i}, delta mse = {mse - last_iteration_mse:.2e}, optimal mse = {optimal_mse:.2e}', end='\r')

            if i == 0:
                # Smooth data
                smoothed_expression = self._apply_smoothing(expression_data, connectivities=current_connectivities,
                                                            denoise_type=self.denoise_type, keep0=self.keep0)
                optimal_mse = last_iteration_mse = mse
                continue
            else:
                # Save the last iteration's connective matrix
                last_iteration_connectivities = current_connectivities

                # Generate the current iteration's connective matrix
                current_connectivities = last_iteration_connectivities.dot(self.connectivities.X)
                self._renormalize(current_connectivities, set_diag=self.set_diag, thresholding=self.thresholding,
                                  weighted=self.weighted)

                # Smooth data with the new connective matrix
                smoothed_expression = self._apply_smoothing(expression_data, connectivities=current_connectivities,
                                                            denoise_type=self.denoise_type, keep0=self.keep0)

            # Check and see if MSE has increased
            # If it hasn't, run the next iteration

            if self.run2best and mse > optimal_mse:
                self.opt_connectivities = last_iteration_connectivities
                self.opt_iterations = i - 1
                break
            elif self.run2best:
                optimal_mse = last_iteration_mse = mse
            elif fixed_iterations and mse > optimal_mse:
                last_iteration_mse = mse
            elif fixed_iterations:
                self.opt_connectivities = current_connectivities
                self.opt_iterations = i
                optimal_mse = last_iteration_mse = mse

        return prediction

    def plot(self, ax=None, metric='mse', verbose=None, skipfirst=True):
        """Simple overview plot of fit performance

        :param ax: a figure axis, Default None
        :param metric: one of 'mse' or 'r2'
        :param verbose: Should we use annotation on plot. If None will use the DEWAKSS default. Default None
        :returns: (figure, ax) if ax is not None else (None, ax)
        :rtype: matplotlib axis

        """
        pass

    def var(self, data, Xmean=None):
        """Calculate the variance of the noise - denoised expression.

        :param data: AnnData object or matrix
        :param Xmean: If precacluated the local mean expression matrix, else it will be calculated from the input data.
            Default None.
        :returns: X - dewakss(X)
        :rtype: ndarray, sparse
        """

        _check_is_fitted(self, attributes='opt_connectivities')

        if not isinstance(data, DEWAKSSData):
            data = DEWAKSSData(data)

        if Xmean is None and self.create_layer not in data.anndata.layers:
            Xmean = self._apply_smoothing(data.X, self.opt_connectivities, denoise_type=self.denoise_type,
                                          keep0=self.keep0)

        deltaX = DEWAKSSData(data.X - Xmean.X)
        feature_variance = deltaX.var(axis=0)
        data_variance = deltaX.var(axis=1)

        return feature_variance, data_variance

    def transform(self, data, transformtype="mean"):
        """Transform function for DEWAKSS

        Will transform an input matrix with the fitted affinity kernal. If AnnData object will add the transformed data to the create_layer to layer.

        :param data: AnnData or matrix like data.
        :param copy: if the AnnData object should be handled in place or returned.
        :returns: matrix data or None
        :rtype: nparray, sparse, AnnData, None

        """

        data = DEWAKSSData(data)
        data.uns['dewakss'] = {self.create_layer: {'opt_connectivities': self.opt_connectivities,
                                                    "opt_iterations": self.opt_iterations,
                                                    "mode": self.mode,
                                                    'layer': self.use_layer}}


        # Transform data
        xformed_data = self._apply_smoothing(data, self.opt_connectivities,
                                             denoise_type=transformtype, keep0=self.keep0)

        # Check and see if it should be cast to a dense array and then put it into the original AnnData object
        xformed_data.check_density(threshold=self.max_dense)
        data.anndata.layers[self.create_layer] = xformed_data.X

        # Return the AnnData object
        return data.anndata


class DEWAKSSData(object):
    """
    Data manager. Accepts an AnnData object, numpy array, or scipy sparse matrix
    Stores data internally as an AnnData object
    Exposes X and uns properties directly and the AnnData object through the anndata property
    Specifically does not permit overwriting the AnnData object

    Mostly this handles doing the same thing in different ways to dense or sparse data
    """

    _data_object = None
    _data_object_sparse = False
    _data_object_layer = "X"

    @property
    def X(self):
        return getattr(self._data_object, self._data_object_layer)

    @X.setter
    def X(self, val):
        setattr(self._data_object, self._data_object_layer,  val)

    @property
    def X_dense(self):
        if self._data_object_sparse:
            return self.X.toarray()
        else:
            return self.X

    @property
    def uns(self):
        return self._data_object.uns

    @property
    def anndata(self):
        return self._data_object

    @property
    def n_neighbors(self):
        if 'neighbors' in self.uns:
            return self.uns['neighbors']['params']['n_neighbors']
        else:
            _warnings.warn("neighborhood not computed, is now run with default parameters", UserWarning)
            _sc.pp.neighbors(self.anndata)
            return self.uns['neighbors']['params']['n_neighbors']

    def __init__(self, data, layer="X", force_csr=True, copy=False):

        if isinstance(data, DEWAKSSData):
            raise RecursionError("Don't give a DEWAKSSData object to a new DEWAKSSData object")

        if copy:
            data = data.copy()

        if isinstance(data, _AnnData):
            self._data_object_sparse = _issparse(data.X)
            self._data_object = data
        elif _issparse(data):
            self._data_object_sparse = True
            self._data_object = _AnnData(X=data)
        else:
            self._data_object = _AnnData(X=data)

        self._data_object_layer = layer

        if force_csr:
            self._enforce_csr()

    # EVERYTHING BETWEEN HERE AND THE NEXT BLOCK COMMENT CHANGES IN PLACE #

    def make_dense(self):
        if self._data_object_sparse:
            self.X = self.X.toarray()
            self._data_object_sparse = False

    def check_density(self, threshold=0.25):
        if self._data_object_sparse:
            sparsity = self.X.nnz / (self.X.shape[0] * self.X.shape[1])
            if sparsity > threshold:
                self.make_dense()

    def eliminate_zeros(self):
        if self._data_object_sparse:
            self.X.eliminate_zeros()

    def _enforce_csr(self):
        if _issparse(self.X) and not _scs.isspmatrix_csr(self.X):
            self.X = _scs.csr_matrix(self.X)

    def multiply(self, val, **kwargs):
        if self._data_object_sparse and _issparse(val):
            self.X = self.X.multiply(val, **kwargs)
            self.X.eliminate_zeros()
        elif self._data_object_sparse:
            self.X = self.X.multiply(_scs.csr_matrix(val))
        else:
            self.X = _np.multiply(self.X, val, **kwargs)

    def exp(self, modifier_value=1):
        if self._data_object_sparse and modifier_value != 1:
            self.X.data = _np.exp(modifier_value * self.X.data)
        elif self._data_object_sparse:
            self.X.data = _np.exp(self.X.data)
        elif modifier_value != 1:
            self.X = _np.exp(modifier_value * self.X)
        else:
            self.X = _np.exp(self.X)

    def make_symmetric(self):
        self.X = self.X + self.X.T

    def fill_diagonal(self, val=0):
        if val is None:
            return
        elif self._data_object_sparse:
            self.X.setdiag(val)
            if val == 0:
                self.X.eliminate_zeros()
        else:
            _np.fill_diagonal(self.X, val)

    def threshold(self, threshold, fill_value=0, func=_np.less):
        if self._data_object_sparse:
            self.X.data[func(self.X.data, threshold)] = fill_value
            if fill_value == 0:
                self.X.eliminate_zeros()
        else:
            self.X[func(self.X, threshold)] = fill_value

    def trim_to_nnz(self, nnz):
        """
        Trim X to a maximum number of non-zeros (by discarding the lowest values)
        Do this in-place
        :param nnz: int
        """
        if self._data_object_sparse:
            flat_data = self.X.data
            for i in range(self.X.shape[0]):
                start, stop = self.X.indptr[i], self.X.indptr[i+1]
                current_nnz = stop - start
                if current_nnz <= nnz:
                    pass
                else:
                    row_data = flat_data[start:stop]
                    flat_data[start + _np.argsort(row_data)[:current_nnz-nnz]] = 0
            self.X.data = flat_data
            self.X.eliminate_zeros()
        else:
            for i in range(self.X.shape[0]):
                current_row = self.X[i, :]
                current_row_nnz = _np.sum(current_row != 0)
                if current_row_nnz <= nnz:
                    pass
                else:
                    self.X[i, _np.argsort(current_row)[:-nnz]] = 0

    def astype(self, change_type):
        self.X = self.X.astype(change_type)

    # EVERYTHING BELOW HERE RETURNS INSTEAD OF CHANGING IN-PLACE #

    def mean_of_nnz(self, axis=1):
        if self._data_object_sparse:
            nnz = self.X.getnnz(axis=axis)
            nnz[nnz == 0] = 1
            return _np.asarray(self.X.sum(axis=axis).A1 / nnz)
        else:
            nnz = (self.X != 0).sum(axis=axis)
            nnz[nnz == 0] = 1
            return self.X.sum(axis=axis) / nnz

    def copy(self):
        return DEWAKSSData(self.anndata.copy(), layer=self._data_object_layer)

    def dot(self, right):
        if isinstance(right, DEWAKSSData):
            right = right.X

        if self._data_object_sparse:
            return DEWAKSSData(self.X.dot(right))
        elif _issparse(right):
            return DEWAKSSData(self.X @ right)
        else:
            return DEWAKSSData(_np.dot(self.X, right))

    def mse(self, true_values=None):
        if true_values is not None and isinstance(true_values, DEWAKSSData):
            true_values = true_values.X

        if self._data_object_sparse and true_values is None:
            return _np.asarray(self.X.power(2).sum())
        elif true_values is None:
            return _np.sum(_np.power(self.X, 2))
        elif self._data_object_sparse and _issparse(true_values):
            return _np.asarray((self.X - true_values).power(2).sum())
        else:
            return _np.sum(_np.power(self.X - true_values, 2))

    def r2(self, true_values=None):
        if true_values is not None and isinstance(true_values, DEWAKSSData):
            true_values = true_values.X

        if true_values is None:
            return 0
        elif _issparse(true_values):
            return _r2(self.X_dense, true_values.A)
        else:
            return _r2(self.X_dense, true_values)

    def var(self, axis=0):
        if self._data_object_sparse:
            return _np.asarray(self.X.power(2).mean(axis=axis) - _np.power(self.X.mean(axis=axis), 2)).flatten()
        else:
            return _np.var(self.X, axis=axis)

    def sum(self, axis=None):
        if self._data_object_sparse:
            return _np.asarray(self.X.sum(axis=axis)).flatten()
        else:
            return _np.sum(self.X, axis=axis)
