from scipy.sparse import csr_matrix as _csr, csc_matrix as _csc, coo_matrix as _coo, hstack, vstack, bmat
from scipy.sparse import isspmatrix_csc as _iscsc, isspmatrix_csr as _iscsr, issparse as _issparse
from scipy.sparse.construct import _compressed_sparse_stack

import numpy as np
import math

import pathos
import pathos.helpers

ALL_CORES = pathos.helpers.cpu_count()


class MatrixMultiplication(object):

    dense_block_size = 1e8
    n_threads = ALL_CORES
    max_sparsity = 0.4

    @classmethod
    def matmul(cls, arr_a, arr_b):

        if arr_a.shape[1] != arr_b.shape[0]:
            raise ValueError("Matrix dimensions {a} & {b} are not aligned for dot product".format(a=arr_a.shape,
                                                                                                  b=arr_b.shape))

        a_sparsity = _get_sparsity(arr_a)
        b_sparsity = _get_sparsity(arr_b)
        mean_sparsity = np.mean((a_sparsity, b_sparsity))

        # If both dense, use the numpy dot function
        if not _issparse(arr_a) and not _issparse(arr_b):
            return cls._np_matmul(arr_a, arr_b)

        # If both sparse and not very dense, use the scipy dot function
        elif _issparse(arr_a) and _issparse(arr_b) and mean_sparsity < 0.005:
            return cls._sp_matmul(arr_a, arr_b)

        # If both sparse use then use chunking & threading
        elif _issparse(arr_a) and _issparse(arr_b):
            return cls._sp_matmul_through_sparse(arr_a, arr_b)

        # If only one is sparse and its not very sparse, cast it to dense and use the numpy dot function
        elif _issparse(arr_a) and not _issparse(arr_b) and mean_sparsity > 0.65:
            return cls._np_matmul(arr_a.todense(), arr_b)
        elif not _issparse(arr_a) and _issparse(arr_b) and mean_sparsity > 0.65:
            return cls._np_matmul(arr_a, arr_b.todense())

        # Otherwise if only one is sparse chunk it and give it to numpy dot
        elif _issparse(arr_a) != _issparse(arr_b):
            return cls._sp_matmul_through_dense(arr_a, arr_b)

        # I think this covers all possibilities so raise an error if you got here
        else:
            raise NotImplementedError("This combination of array args has not been properly implemented. Apparently.")

    @staticmethod
    def _np_matmul(arr_a, arr_b):
        return np.dot(arr_a, arr_b)

    @staticmethod
    def _sp_matmul(arr_a, arr_b):
        return arr_a.dot(arr_b)

    @classmethod
    def _sp_matmul_through_dense(cls, arr_a, arr_b):

        a_chunks, a_size = cls._get_chunk_size(arr_a, axis=0)
        b_chunks, b_size = cls._get_chunk_size(arr_b, axis=1)

        if _issparse(arr_a) and not _iscsr(arr_a):
            arr_a = _csr(arr_a)
        if _issparse(arr_b) and not _iscsc(arr_b):
            arr_b = _csc(arr_b)

        _sp_for_vstack = []
        for i in range(a_chunks):
            _sp_for_hstack = []

            if a_size is not None:
                a_start, a_stop = i * a_size, min((i + 1) * a_size, arr_a.shape[0])
                a_dense = arr_a[a_start:a_stop, :].todense()
            else:
                a_dense = arr_a

            for j in range(b_chunks):
                if b_size is not None:
                    b_start, b_stop = j * b_size, min((j + 1) * b_size, arr_b.shape[1])
                    b_dense = arr_b[:, b_start:b_stop].todense()
                else:
                    b_dense = arr_b

                _sp_for_hstack.append(_csc(np.dot(a_dense, b_dense)))
                del b_dense
            _sp_for_vstack.append(_compressed_sparse_stack(_sp_for_hstack, 1).tocsr())
            del a_dense

        return _compressed_sparse_stack(_sp_for_vstack, 0)

    @classmethod
    def _sp_matmul_through_sparse(cls, arr_a, arr_b):
        if not _issparse(arr_a) or not _issparse(arr_b):
            raise ValueError("_sp_matmul_through_sparse only accepts sparse matrices")

        if not _iscsr(arr_a):
            arr_a = _csr(arr_a)
        if not _iscsc(arr_b):
            arr_b = _csc(arr_b)

        _, a_chunks, _, _ = _define_size_and_n(arr_a, arr_b, cls.n_threads)

        # Chunk data and then put them into a threadpool for dot product
        with pathos.threading.ThreadPool(processes=cls.n_threads) as tp:
            chunks = {i: sparr for i, sparr in tp.amap(_chunk_process, _sp_chunk(arr_a, arr_b, cls.n_threads)).get()}
            chunks = [chunks[i] for i in range(a_chunks)]

        overall_sparsity = np.mean(list(map(lambda x: _get_sparsity(x), chunks)))

        if overall_sparsity > cls.max_sparsity:
            return np.vstack(map(lambda x: _clean_to_dense(x), chunks))
        else:
            return _compressed_sparse_stack(chunks, 0)

    @classmethod
    def _get_chunk_size(cls, arr, axis=0):
        if _issparse(arr) and arr.size < cls.dense_block_size:
            return 1, arr.shape[axis]
        elif _issparse(arr):
            chunk_count = min(math.ceil(arr.size / cls.dense_block_size), arr.shape[axis])
            chunk_size = math.ceil(arr.shape[axis] / chunk_count)
            return chunk_count, chunk_size
        else:
            return 1, None


def _get_sparsity(arr):
    arr_size = arr.shape[0] * arr.shape[1]
    if _issparse(arr) and arr_size > 0:
        return arr.nnz / arr_size
    elif _issparse and arr_size == 0:
        return 0.
    else:
        return 1.


def _define_size_and_n(arr_a, arr_b, n_cores):
    a_size = math.ceil(arr_a.shape[0] / n_cores)
    b_size = math.ceil(arr_b.shape[1] / n_cores)
    a_chunks = max(math.ceil(arr_a.shape[0] / a_size), 1)
    b_chunks = max(math.ceil(arr_b.shape[1] / b_size), 1)

    return a_size, a_chunks, b_size, b_chunks


def _sp_chunk(arr_a, arr_b, n_cores):
    a_size, a_chunks, b_size, b_chunks = _define_size_and_n(arr_a, arr_b, n_cores)

    for i in range(a_chunks):
        if a_size > 1:
            a_start, a_stop = i * a_size, min((i + 1) * a_size, arr_a.shape[0])
            a_sp = arr_a[a_start:a_stop, :]
        else:
            a_sp = arr_a[i, :]

        b_sp = arr_b[:]

        yield i, a_sp, b_sp


def _chunk_process(args):
    i, a_sp, b_sp = args
    sp_dot = a_sp.dot(b_sp)
    sp_dot.eliminate_zeros()
    return i, sp_dot.tocsr()


def _clean_to_dense(arr):
    dense = arr.todense()
    _del_arr(arr)
    return dense


def _del_arr(sp_arr):
    try:
        del sp_arr.data
    except AttributeError:
        pass
    try:
        del sp_arr.indices
    except AttributeError:
        pass
    try:
        del sp_arr.indptr
    except AttributeError:
        pass
