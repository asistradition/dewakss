import unittest
import numpy as np
import scipy.sparse as sparse
import numpy.testing as npt

from dewakss.matmul import MatrixMultiplication as MatMul

# 4 x 10
TEST_MATRIX = np.array([list(range(10)),
                       [0, 1] * 5,
                       [0] * 10,
                       [0] * 9 + [1]])

TEST_SPARSE = sparse.csr_matrix(TEST_MATRIX)

TEST_CONNECT_SELF = np.eye(4, dtype=int)
TEST_CONNECT_ALL = np.ones((4, 4), dtype=int)

TEST_ALL_CORRECT = np.dot(TEST_CONNECT_ALL, TEST_MATRIX)


class TestMatMulNoChunk(unittest.TestCase):

    def test_dense(self):

        npt.assert_array_equal(MatMul.matmul(TEST_CONNECT_SELF, TEST_MATRIX), TEST_MATRIX)
        npt.assert_array_equal(MatMul.matmul(TEST_CONNECT_ALL, TEST_MATRIX), TEST_ALL_CORRECT)

    def test_half_sparse(self):

        npt.assert_array_equal(MatMul.matmul(TEST_CONNECT_SELF, TEST_SPARSE).A, TEST_MATRIX)
        npt.assert_array_equal(MatMul.matmul(TEST_CONNECT_ALL, TEST_SPARSE).A, TEST_ALL_CORRECT)

    def test_all_sparse(self):

        npt.assert_array_equal(MatMul.matmul(sparse.csr_matrix(TEST_CONNECT_SELF), TEST_SPARSE).A, TEST_MATRIX)
        npt.assert_array_equal(MatMul.matmul(sparse.csr_matrix(TEST_CONNECT_ALL), TEST_SPARSE).A, TEST_ALL_CORRECT)


class TestMatMulChunking(TestMatMulNoChunk):

    def setUp(self):
        MatMul.dense_block_size = 10

    def tearDown(self):
        MatMul.dense_block_size = 1e7


class Test_sp_matmul_through_sparse(unittest.TestCase):

    def test_sp_matmul_sparse(self):
        arr = MatMul._sp_matmul_through_sparse(sparse.csr_matrix(TEST_CONNECT_ALL), TEST_SPARSE)
        npt.assert_array_equal(arr.A, TEST_ALL_CORRECT)
