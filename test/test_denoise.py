import unittest
import numpy as np
import scipy.sparse as sparse
from scipy.sparse import csr_matrix as _csr
import anndata
import numpy.testing as npt

from dewakss.denoise import DEWAKSSData, DEWAKSS

# 4 x 10
TEST_MATRIX = np.array([list(range(10)),
                       [0, 1] * 5,
                       [0] * 10,
                       [0] * 9 + [1]])

TEST_SPARSE = sparse.csr_matrix(TEST_MATRIX)

TEST_CONNECT_SELF = np.eye(4, dtype=int)
TEST_CONNECT_ALL = np.ones((4, 4), dtype=int)

TEST_ALL_CORRECT = np.dot(TEST_CONNECT_ALL, TEST_MATRIX)

class TestDenoiseDense(unittest.TestCase):

    def setUp(self):
        self.data = DEWAKSSData(TEST_MATRIX)
        self.connect_self = DEWAKSSData(TEST_CONNECT_SELF)
        self.connect_all = DEWAKSSData(TEST_CONNECT_ALL)

    def test_row_stochastic(self):
        DEWAKSS._row_stochastic(self.data)
        self.assertListEqual(np.asarray(self.data.X.sum(axis=1)).astype(int).flatten().tolist(), [1, 1, 0, 1])

    def test_smoothing_self_only(self):
        npt.assert_array_equal(self.connect_self.dot(TEST_MATRIX).X, TEST_MATRIX)
        npt.assert_array_equal(self.connect_self.dot(TEST_SPARSE).X, TEST_MATRIX)

    def test_smoothing_all(self):
        npt.assert_array_equal(self.connect_all.dot(TEST_MATRIX).X, TEST_ALL_CORRECT)


class TestDenoiseSparse(TestDenoiseDense):

    def setUp(self):
        self.data = DEWAKSSData(TEST_SPARSE)
        self.connect_self = DEWAKSSData(_csr(TEST_CONNECT_SELF))
        self.connect_all = DEWAKSSData(_csr(TEST_CONNECT_ALL))

    def test_smoothing_self_only(self):
        npt.assert_array_equal(self.connect_self.dot(TEST_SPARSE).X.A, TEST_MATRIX)
        npt.assert_array_equal(self.connect_self.dot(TEST_MATRIX).X, TEST_MATRIX)

    def test_smoothing_all(self):
        npt.assert_array_equal(self.connect_all.dot(TEST_SPARSE).X.A, TEST_ALL_CORRECT)
        npt.assert_array_equal(self.connect_all.dot(TEST_MATRIX).X, TEST_ALL_CORRECT)
