import unittest
import numpy as np
import scipy.sparse as sparse
import anndata
import numpy.testing as npt

from dewakss.denoise import DEWAKSSData

# 4 x 10
TEST_MATRIX = np.array([list(range(10)),
                       [0, 1] * 5,
                       [0] * 10,
                       [0] * 9 + [1]])

TEST_SPARSE = sparse.csr_matrix(TEST_MATRIX)


class TestCreate(unittest.TestCase):

    def test_init_dense(self):
        data = DEWAKSSData(TEST_MATRIX)

        self.assertFalse(data._data_object_sparse)
        self.assertTupleEqual(data.X.shape, TEST_MATRIX.shape)

    def test_init_sparse(self):
        data = DEWAKSSData(TEST_SPARSE)

        self.assertTrue(data._data_object_sparse)
        self.assertTrue(sparse.isspmatrix_csr(data.X))
        self.assertTupleEqual(TEST_SPARSE.shape, data.X.shape)

    def test_init_anndata_dense(self):
        data = DEWAKSSData(anndata.AnnData(TEST_MATRIX))

        self.assertFalse(data._data_object_sparse)
        self.assertTupleEqual(data.X.shape, TEST_MATRIX.shape)

    def test_init_anndata_sparse(self):
        data = DEWAKSSData(anndata.AnnData(TEST_SPARSE))

        self.assertTrue(data._data_object_sparse)
        self.assertTrue(sparse.isspmatrix_csr(data.X))
        self.assertTupleEqual(TEST_SPARSE.shape, data.X.shape)

    def test_properties(self):
        data = DEWAKSSData(TEST_SPARSE)
        data.X = data.X

        with self.assertRaises(AttributeError):
            data.uns = data.uns

        with self.assertRaises(AttributeError):
            data.anndata = data.anndata


class TestDense(unittest.TestCase):

    def setUp(self):
        self.data = DEWAKSSData(TEST_MATRIX)

    def test_multiply(self):

        self.data.multiply(2)
        npt.assert_array_equal(TEST_MATRIX * 2, self.data.X)

        self.data.multiply(np.array([0, 2, 2, 2])[:, np.newaxis])
        npt.assert_array_equal(self.data.X.sum(axis=1), np.array([0, 20, 0, 4]))

        self.data.multiply(np.array([0] * 10)[np.newaxis, :])
        self.assertEqual(np.sum(self.data.X), 0)

    def test_fill_diag(self):

        self.data.fill_diagonal(val=100)
        self.assertEqual(self.data.X[0, 0], 100)
        self.assertEqual(self.data.X[1, 1], 100)
        self.assertEqual(self.data.X[2, 2], 100)
        self.assertEqual(self.data.X[3, 3], 100)

    def test_threshold(self):

        self.data.threshold(5)
        self.assertEqual(np.sum(self.data.X != 0), 5)

        self.data.threshold(7, func=np.less_equal)
        self.assertEqual(np.sum(self.data.X != 0), 2)

    def test_mean_of_nnz(self):

        rmean = self.data.mean_of_nnz(axis=1)
        npt.assert_array_equal(rmean, np.array([5, 1, 0, 1]))

        colmean = self.data.mean_of_nnz(axis=0)
        npt.assert_almost_equal(colmean, np.array([0., 1., 2., 2., 4., 3., 6., 4., 8., 3.666667]), decimal=3)

    def test_row_nnz(self):

        self.data.trim_to_nnz(4)
        npt.assert_array_equal(np.sum(self.data.X != 0, axis=1), np.array([4, 4, 0, 1]))

    def test_dot(self):

        dot = self.data.dot(np.eye(10))
        npt.assert_array_equal(dot.X, self.data.X)

        dot_2 = self.data.dot(sparse.eye(10, format="csr"))
        npt.assert_array_equal(dot_2.X, self.data.X)

    def test_mse(self):

        self.assertEqual(self.data.mse(), np.power(TEST_MATRIX, 2).sum())
        self.assertEqual(self.data.mse(TEST_MATRIX), 0)
        self.assertEqual(self.data.mse(TEST_SPARSE), 0)

    def test_r2(self):

        self.assertEqual(self.data.r2(), 0)
        self.assertEqual(self.data.r2(TEST_MATRIX), 1)
        self.assertEqual(self.data.r2(TEST_SPARSE), 1)

    def test_var(self):

        npt.assert_array_equal(self.data.var(), np.array([0., 0.25, 0.75, 1.5,  3., 4.25, 6.75, 8.5, 12., 13.1875]))


class TestSparse(TestDense):

    def setUp(self):
        self.data = DEWAKSSData(TEST_SPARSE)

    def test_multiply(self):

        self.data.multiply(2)
        npt.assert_array_equal(TEST_MATRIX * 2, self.data.X.A)

        self.data.multiply(np.array([0, 2, 2, 2])[:, np.newaxis])
        npt.assert_array_equal(np.array(self.data.X.sum(axis=1)).reshape(4,), np.array([0, 20, 0, 4]))

        self.data.multiply(np.array([0] * 10)[np.newaxis, :])
        self.assertEqual(np.sum(self.data.X), 0)

    def test_row_nnz(self):

        self.data.trim_to_nnz(4)
        npt.assert_array_equal(np.array(np.sum(self.data.X != 0, axis=1)).reshape(4,), np.array([4, 4, 0, 1]))

    def test_dot(self):

        dot = self.data.dot(np.eye(10))
        npt.assert_array_equal(dot.X, self.data.X.A)

        dot_2 = self.data.dot(sparse.eye(10, format="csr"))
        npt.assert_array_equal(dot_2.X.A, self.data.X.A)